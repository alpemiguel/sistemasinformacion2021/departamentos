<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departamentos de la base de datos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear nuevo departamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'dept_no',
            'dnombre',
            'loc',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
